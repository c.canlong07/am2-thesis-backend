const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");
const EmpRouter = require('./routes/employee');
const users = require("./routes/users");
require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

// middleware
app.use(cors());
app.use(express.json());

// app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req, res) => {
  res.send('blahgh');
})

// mogodb connection
const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});

const connection = mongoose.connection;
connection.once('open', ()=> {
    console.log("MongoDB database connection is established.");
}); 
// Passport middleware
app.use(passport.initialize());

// Passport config
require("./config/passport")(passport);

app.use('/employee', EmpRouter);

// Routes
app.use('/api/users', users);

app.listen(port, ()=> {
    console.log(`Server is running in port : ${port}`);
})