const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const budgetSchema = new Schema({

   //    user: {
   //       type: Schema.ObjectId,
   //       required: true,
   //       trim: true,
   //       ref: 'users',
   //   },
   budgetDate: {
      type: String,
      required: true,
      trim: true,
   },
   budgetNote: {
      type: String,
      required: true,
      trim: true
   },
   budget: {
      type: Number,
      required: true,
      trim: true
   },
   deleted: {
      type: String,
      trim: true
   },
   usedBudget: {
      type: Object,
      trim: true
   },
   shared: {
      type: String,
      trim: true
   },
   sharedBudget: {
      type: String,
      trim: true
   },
   sharedBy: {
      type: String,
      trim: true
   },

}, {
   timestamps: true
});

const Budgets = mongoose.model('budget', budgetSchema);

module.exports = Budgets;