const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeSchema = new Schema({

   firstname: {
      type: String,
      required: true,
      trim: true
   },
   lastname: {
      type: String,
      required: true,
      trim: true
   },
   address: {
      type: String,
      required: true,
      trim: true
   },
   department: {
      type: String,
      required: true,
      trim: true
   },
   email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      match: [/.+\@.+\..+/, 'Please fill a valid email address'],
      unique: true
   },
   cpnumber: {
      type: String,
      required: true,
      trim: true
   },
   password: {
      type: String,
      required: true,
      trim: true
   },
   role: {
      type: String,
      required: true,
      trim: true
   },
   deleted: {
      type: String,
      trim: true
   },

}, {
   timestamps: true
});

const Employee = mongoose.model('user', employeeSchema);

module.exports = Employee;