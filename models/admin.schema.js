const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const AdminSchema = new Schema({
  morningFrom: {
    type: String,
    trim: true
  },
  morningTo: {
    type: String,
    trim: true
  },
  afternoonFrom: {
    type: String,
    trim: true
  },
  afternoonTo: {
    type: String,
    trim: true
  },
  
});

module.exports = mongoose.model("admin", AdminSchema);

