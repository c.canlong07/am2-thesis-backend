const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const mealsSchema = new Schema({

   //    user: {
   //       type: Schema.ObjectId,
   //       required: true,
   //       trim: true,
   //       ref: 'users',
   //   },
   mealName: {
      type: String,
      required: true,
      trim: true
   },
   mealDesc: {
      type: String,
      required: true,
      trim: true
   },
   mealIng: {
      type: String,
      required: true,
      trim: true
   },
   // mealDetails: {
   //    type:String,
   //    required: true,
   //    trim: true
   // },
   mealDate: {
      type: String,
      required: true,
      trim: true
   },
   mealServedEst: {
      type: Number,
      required: true,
      trim: true
   },
   mealTotalPrice: {
      type: Number,
      required: true,
      trim: true
   },
   mealTotalPerEmployee: {
      type: Number,
      required: true,
      trim: true
   },
   mealType: {
      type: String,
      required: true,
      trim: true
   },
   mealCategory: {
      type: String,
      required: true,
      trim: true
   },
   mealServed: {
      type: Number,
      // required: true,
      trim: true
   },
   defaultMeal: {
      type: String,
      trim: true,
   },
   deleted: {
      type: String,
      trim: true
   },
   budget: {
      type: Schema.ObjectId,
      required: true,
      trim: true,
      ref: 'budget',
   },
   mealRecommendationData: {
      type: Object,
      // required: true,
      trim: true
   },
   savedRecommendationData: {
      type: Object,
      // required: true,
      trim: true
   },
   originalMealData: {
      type: Object,
      // required: true,
      trim: true
   },
   recommendIsON: {
      type: String,
      trim: true,
   }

}, {
   timestamps: true
});

const Meals = mongoose.model('meal', mealsSchema);

module.exports = Meals;