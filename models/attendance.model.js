const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const attendanceSchema = new Schema({

    user: {
        type: Schema.ObjectId,
        required: true,
        trim: true,
        ref: 'users',
    },
    timeIn: {
        type: Object,
        // required: true,
        trim: true
    },
    timeOut: {
        type: Object,
        // required: true,
        trim: true
    },
    dateRecorded: {
        type: String,
        required: true,
        trim: true
    },


}, {
    timestamps: true
});

const Attendance = mongoose.model('attendance', attendanceSchema);

module.exports = Attendance;