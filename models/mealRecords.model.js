const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const mealRecordSchema = new Schema({

  user: {
    type: Schema.ObjectId,
    required: true,
    trim: true,
    ref: 'users',
  },
  meal: {
    type: Schema.ObjectId,
    required: true,
    trim: true,
    ref: 'meal',
  },
  mealType: {
    type: String,
    required: true,
    trim: true
  },
  rating: {
    type: Number,
    //  required: true,
    trim: true
  },
  feedback: {
    type: String,
    // required: true,
    trim: true
  },
  feedback2: {
    type: String,
    // required: true,
    trim: true
  },
  ingToAdd: {
    type: Object,
    // required: true,
    trim: true
  },
  ingToRemove: {
    type: Object,
    // required: true,
    trim: true
  },
  availing: {
    type: String,
    // required: true,
    trim: true
  },
  mealCategory: {
    type: String,
    // required: true,
    trim: true
  },
  availed: {
    type: String,
    // required: true,
    trim: true
  }

}, {
  timestamps: true
});

const MealRecords = mongoose.model('mealRecord', mealRecordSchema);

module.exports = MealRecords;