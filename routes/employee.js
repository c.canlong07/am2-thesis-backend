const router = require('express').Router();
let Employee = require('../models/employee.model');
let Attendance = require('../models/attendance.model');
let Meals = require('../models/meal.model');
let MealRecords = require('../models/mealRecords.model');
let Time = require('../models/admin.schema');
let Budgets = require('../models/budget.model');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
var async = require('async');
var _ = require('lodash');
const moment = require('moment')

// Load input validation
// const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");

router.route("/login").post((req, res) => {

    //Form Valdiation
    const { errors, isValid } = validateLoginInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    //Find user by Email
    User.findOne({ email }).then(user => {
        if (!user) {
            return res.status(404).json({ emailnotfound: "Email not found" });
        }

        // Check password
        // bcrypt.compare(password, user.password).then(isMatch => {
        if (password === user.password) {
            // Create JWT Payload
            const payload = {
                id: user.id,
                name: user.name
            };

            // Sign token
            jwt.sign(
                payload,
                keys.secretOrKey,
                {
                    expiresIn: 31556926
                },
                (err, token) => {
                    res.json({
                        success: true,
                        token: "Bearer " + token,
                        data: user
                    });
                }
            );
        } else {
            return res
                .status(400)
                .json({ passwordincorrect: "Password incorrect" });
        }
        //   });
    });
});

// Load User model
const User = require("../models/user.schema");
var ObjectId = require('mongodb').ObjectID;

// home
//all employee list
router.route('/').get((req, res) => {
    let param = req.query.page;
    Employee.find({ "deleted": { $ne: 'true' } }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));
});

//get attendance
router.route('/att').get((req, res) => {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    Attendance.find({ "dateRecorded": { $gte: startDate, $lte: endDate } }).sort({ updatedAt: -1 }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});


//get mealRecords
router.route('/mealRecords').get((req, res) => {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    MealRecords.find({ "createdAt": { $gte: startDate, $lte: endDate } }).sort({ createdAt: -1 }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).populate({
        path: 'meal',
        select: { mealName: 1, mealDesc: 1, mealIng: 1, mealType: 1, _id: 1, mealServedEst: 1 },
    })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});
//get mealRecords
router.route('/usermealRecords').get((req, res) => {
    let userID = req.query.userID;
    let mealID = req.query.mealID;
    MealRecords.find({ "user": { $eq: ObjectId(userID) }, "meal": { $eq: ObjectId(mealID) } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get mealRecords
router.route('/adminmealRecords').get((req, res) => {
    let mealID = req.query.mealID;
    MealRecords.find({ "meal": { $eq: ObjectId(mealID) } }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});
//get employee mealRecords
router.route('/employeemealhistory').get((req, res) => {
    let startDate = req.query.startDate;
    let id = req.query.id;
    MealRecords.find({ "user": { $eq: ObjectId(id) } }).sort({ mealDate: -1 }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).populate({
        path: 'meal',
        select: { mealName: 1, mealDate: 1, mealDesc: 1, mealIng: 1, mealType: 1, _id: 1 },
    })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get employee mealRecords
router.route('/employeeallmealhistory').get((req, res) => {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    MealRecords.find({ "createdAt": { $gte: startDate, $lte: endDate } }).sort({ createdAt: -1 }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).populate({
        path: 'meal',
        select: { mealDate: 1, mealName: 1, mealDesc: 1, mealIng: 1, mealType: 1, _id: 1, mealServedEst: 1, mealTotalPrice: 1 },
    })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//add rating
router.route('/employeemealrating').get((req, res) => {
    let rating = req.query.rating;
    let id = req.query.id;
    MealRecords.updateOne(
        { _id: ObjectId(id) },
        { $set: { "rating": rating } }
    ).then(employee => res.json('Succesfully Rate'))
        .catch(err => res.status(400).json('Error :' + err));

});

//add rating feedback
router.route('/employeemealratingfeedback').get((req, res) => {

    let feedback2 = req.query.feedback2;
    let id = req.query.id;
    let rating = req.query.rating;
    let ingToAdd = req.query.ingToAdd ? req.query.ingToAdd : [];
    let ingToRemove = req.query.ingToRemove ? req.query.ingToRemove : [];

    MealRecords.updateOne(
        { _id: ObjectId(id) },
        {
            $set: {
                "feedback2": feedback2, "rating": rating,
                // "ingToAdd": ingToAdd, "ingToRemove": ingToRemove 
            }
        }
    ).then(employee => res.json('Succesfully Rate'))
        .catch(err => res.status(400).json('Error :' + err));

});




//get attendance
router.route('/attall').get((req, res) => {
    Attendance.find().sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get mealrecords
router.route('/mealRecordsall').get((req, res) => {
    MealRecords.find().populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).populate({
        path: 'meal',
        select: { mealName: 1, mealDesc: 1, mealIng: 1, mealType: 1, _id: 1 },
    }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get mealrecords
router.route('/mealRecordsall').get((req, res) => {
    MealRecords.find({ "deleted": { $ne: 'true' } }).populate({
        path: 'user',
        select: { firstname: 1, lastname: 1, department: 1, _id: 1 },
    }).populate({
        path: 'meal',
        select: { mealName: 1, mealDesc: 1, mealIng: 1, mealType: 1, _id: 1 },
    }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meals
router.route('/allmeals').get((req, res) => {
    Meals.find({ "deleted": { $ne: 'true' } }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meals
router.route('/allbudgets').get((req, res) => {
    Budgets.find({ "deleted": { $ne: 'true' } }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meal today
router.route('/mealToday').get((req, res) => {
    let startDate = moment(req.query.startDate).format('YYYY-MM-DD');
    let endDate = moment(req.query.endDate).format('YYYY-MM-DD');
    // Meals.find({ "mealDate": { $gte: startDate, $lte: endDate } }).sort({ createdAt: -1 })
    //     .then(employee => res.json(employee))
    //     .catch(err => res.status(400).json('Error :' + err));
    Meals.find({
        'mealDate': { '$regex': startDate, '$options': 'i' },
        "deleted": { $ne: 'true' }
        // $or: [{ 'mealDate': { '$regex': startDate, '$options': 'i' } }, { 'mealDate': { '$regex': endDate, '$options': 'i' } }]

    }).sort({ 'mealDate': -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meal today
router.route('/mealToday2').get((req, res) => {
    let startDate = moment(req.query.startDate).format('YYYY-MM-DD');
    let endDate = moment(req.query.endDate).format('YYYY-MM-DD');

    Meals.find({
        // $or: [{ 'mealDate': { '$regex': startDate, '$options': 'i' } }, { 'mealDate': { '$regex': endDate, '$options': 'i' } }]
        'mealDate': { $gt: startDate, $lt: endDate },
        "deleted": { $ne: 'true' }
    }).sort({ 'mealDate': -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});


//get meal weekly
router.route('/mealWeekly').get((req, res) => {
    let startDate = new Date(req.query.startDate);
    let endDate = new Date(req.query.endDate);
    Meals.find({ "createdAt": { $gte: startDate, $lte: endDate }, "deleted": { $ne: 'true' } }).sort({ createdAt: 1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meal today
router.route('/mealTodaySelected').get((req, res) => {
    let startDate = new Date(req.query.startDate);
    let endDate = new Date(req.query.endDate);
    let id = req.query.id;

    Meals.find({ "createdAt": { $gte: startDate, $lte: endDate }, "_id": { $eq: ObjectId(id) }, "deleted": { $ne: 'true' } }).sort({ createdAt: -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));



});


// add
router.route('/add').post((req, res) => {

    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const address = req.body.address;
    const department = req.body.department;
    const email = req.body.email;
    const cpnumber = req.body.cpnumber;
    const password = req.body.email;
    const role = req.body.role;

    const newEmpDeclaration = new Employee({ firstname, lastname, department, address, email, cpnumber, password, role });

    newEmpDeclaration.save()
        .then(employee => res.json('New Record Added!'))
        .catch(err => res.status(400).json('Error :' + err));

});

// bul add
router.route('/bulkInsert').post((req, res) => {
    // Employee.insertMany(req.body, {ordered: false}).then(employee => res.json('New Record Added!'))
    // .catch(err => res.status(400).json('Error :' + err));
    // Employee.insertMany(req.body, {ordered : false }).then(employee => res.json('New Record Added!'))
    // .catch(err => res.status(400).json('Error :' + err));
    Employee.insertMany(req.body, { ordered: false })
        .then(function (docs) {
            res.send({ data: docs, message: 'success' })
            // console.log(docs.length);
        })
        .catch(function (err) {
            // console.log(err, err.insertedDocs.length);
            // res.json('Record added is '+ err.insertedDocs.length);
            res.send({ data: err, message: 'duplicate' })
        });


    //     ,function (err,data) {

    //     if(err!=null){
    //         return console.log(null);
    //     }
    //     console.log(data.ops);
    // });
    // db.close();

});

// add meals
router.route('/addmeals').post((req, res) => {

    const mealName = req.body.mealName;
    const mealDesc = req.body.mealDesc;
    const mealIng = req.body.mealIng;
    const mealDate = req.body.mealDate;
    const mealServedEst = req.body.mealServedEst;
    const mealTotalPrice = req.body.mealTotalPrice;
    const mealTotalPerEmployee = req.body.mealTotalPerEmployee;
    const mealType = req.body.mealType;
    const mealCategory = req.body.mealCategory;
    const budget = req.body.budget;
    const originalMealData = req.body.originalMealData;
    const mealServed = 0;



    const newMeals = new Meals({ mealName, mealDesc, mealIng, mealDate, mealServedEst, mealTotalPrice, mealTotalPerEmployee, mealType, mealCategory, mealServed, budget, originalMealData });

    newMeals.save()
        .then(employee => res.json('New Record Added!'))
        .catch(err => res.status(400).json('Error :' + err));

});
// details attendance
router.route('/getuserattendance').get((req, res) => {
    let id = req.query.id;
    let startDate = req.query.startDate;
    Attendance.find({ "user": { $eq: ObjectId(id) }, "dateRecorded": { $eq: startDate } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/getmealbybudget').get((req, res) => {
    let id = req.query.id;
    let startDate = req.query.startDate;
    Meals.find({ "budget": { $eq: ObjectId(id) }, "deleted": { $ne: 'true' } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});

// details meals
router.route('/getMeal/:id').get((req, res) => {
    Meals.findById(req.params.id)
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/getMealbydefault').get((req, res) => {
    Meals.find({ "defaultMeal": { $eq: 'true' }, "deleted": { $ne: 'true' } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});



// update meal

router.route('/updateMeal/:id').post((req, res) => {

    Meals.findById((req.params.id))
        .then(employee => {
            employee.mealName = req.body.mealName;
            employee.mealDesc = req.body.mealDesc;
            employee.mealIng = req.body.mealIng;
            employee.mealDate = req.body.mealDate;
            employee.mealServedEst = req.body.mealServedEst;
            employee.mealTotalPrice = req.body.mealTotalPrice;
            employee.mealTotalPerEmployee = req.body.mealTotalPerEmployee;
            employee.mealType = req.body.mealType;
            employee.mealCategory = req.body.mealCategory;
            employee.defaultMeal = req.body.defaultMeal;

            employee.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

});

// meal recommendation
router.route('/updatemealRecommendation/:id').post((req, res) => {
    Meals.findById((req.params.id))
        .then(mealRecommendationData => {
            // attendance.timeIn = req.body.timeIn;
            mealRecommendationData.mealRecommendationData = req.body.mealRecommendationData;

            mealRecommendationData.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});

// meal recommendation
router.route('/updatemealRecommendation2/:id').post((req, res) => {
    Meals.findById((req.params.id))
        .then(savedRecommendationData => {
            // attendance.timeIn = req.body.timeIn;
            savedRecommendationData.savedRecommendationData = req.body.savedRecommendationData;

            savedRecommendationData.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));
});


// delete meal
router.route('/deleteMeal/:id').delete((req, res) => {
    Meals.updateOne(
        { _id: ObjectId(req.params.id) },
        { $set: { "deleted": true } }
    ).then(employee => res.json('Record was deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

// update  meal recommendation
router.route('/recommendison/:id').delete((req, res) => {
    Meals.updateOne(
        { _id: ObjectId(req.params.id) },
        { $set: { "recommendIsON": true } }
    ).then(employee => res.json('Record was deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});


// update  meal recommendation
router.route('/recommendisoff/:id').delete((req, res) => {
    Meals.updateOne(
        { _id: ObjectId(req.params.id) },
        { $set: { "recommendIsON": false } }
    ).then(employee => res.json('Record was deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});
// delete budget
router.route('/deleteBudget/:id').delete((req, res) => {
    Budgets.updateOne(
        { _id: ObjectId(req.params.id) },
        { $set: { "deleted": true } }
    ).then(employee => res.json('Record was deleted.'))
        .catch(err => res.status(400).json('Error: ' + err));
});

// details
router.route('/:id').get((req, res) => {
    Employee.findById(req.params.id)
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});

// delete
router.route('/:id').delete((req, res) => {
    // Employee.findByIdAndDelete(req.params.id)
    // .then(employee => res.json('Record was deleted.'))
    // .catch(err => res.status(400).json('Error: '+ err));
    Employee.updateOne(
        { _id: ObjectId(req.params.id) },
        { $set: { "deleted": true } }
    ).then(employee => res.json('Meal Serve Updated!'))
        .catch(err => res.status(400).json('Error :' + err));
});

// update

router.route('/update/:id').post((req, res) => {

    Employee.findById((req.params.id))
        .then(employee => {
            employee.firstname = req.body.firstname;
            employee.lastname = req.body.lastname;
            employee.address = req.body.address;
            employee.department = req.body.department;
            employee.email = req.body.email;
            employee.cpnumber = req.body.cpnumber;
            employee.password = req.body.password;
            employee.role = req.body.role;

            employee.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

});

// add attendance
router.route('/add/attendance').post((req, res) => {
    const user = req.body.user;
    const timeIn = req.body.timeIn;
    const timeOut = req.body.timeOut;
    const dateRecorded = req.body.dateRecorded;

    const newEmployeeAttendance = new Attendance({ user, timeIn, timeOut, dateRecorded });

    newEmployeeAttendance.save()
        .then(employee => res.json('New Attendance Added!'))
        .catch(err => res.status(400).json('Error :' + err));

});

// add budget
router.route('/add/budget').post((req, res) => {

    const budgetDate = req.body.budgetDate;
    const budgetNote = req.body.budgetNote;
    const budget = req.body.budget;
    const sharedBudget = req.body.sharedBudget;
    const shared = req.body.shared;

    const newEmployeeAttendance = new Budgets({ budgetDate, budgetNote, budget, shared, sharedBudget });

    newEmployeeAttendance.save()
        .then(employee => res.json('New Attendance Added!'))
        .catch(err => res.status(400).json('Error :' + err));

});

// details budget
router.route('/getBudget/:id').get((req, res) => {
    Budgets.findById(req.params.id)
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add/usedremainingbudget').get((req, res) => {

    let id = req.query.id;
    let remainingBudget = req.query.remainingBudget;


    Budgets.updateOne(
        { _id: ObjectId(id) },
        { $set: { usedBudget: [remainingBudget] } }
    ).then(employee => res.json('Succesfully Update Budget'))
        .catch(err => res.status(400).json('Error :' + err));

});

router.route('/remove/removedremainingbudget').get((req, res) => {

    let id = req.query.id;
    let remainingBudget = req.query.remainingBudget;


    Budgets.updateOne(
        { _id: ObjectId(id) },
        { $unset: { usedBudget: '' } }
    ).then(employee => res.json('Succesfully Update Budget'))
        .catch(err => res.status(400).json('Error :' + err));

});




// update budget

router.route('/updateBudget/:id').post((req, res) => {

    Budgets.findById((req.params.id))
        .then(employee => {
            employee.budgetNote = req.body.budgetNote;
            employee.budget = req.body.budget;

            employee.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

});

//get budget today
router.route('/check/budToday').get((req, res) => {
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    Budgets.find({ "createdAt": { $gte: startDate, $lte: endDate } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get budget for specific day
router.route('/check/budSpecificToday').get((req, res) => {
    let id = req.query.id;
    let endDate = req.query.endDate;
    Budgets.find({ "_id": { $eq: ObjectId(id) } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

//get mealRecords
router.route('/check/weeklybudget').get((req, res) => {
    let startDate = moment(req.query.startDate).format('YYYY-MM-DD');
    let endDate = moment(req.query.endDate).format('YYYY-MM-DD');

    Budgets.find({
        // $or: [{ 'mealDate': { '$regex': startDate, '$options': 'i' } }, { 'mealDate': { '$regex': endDate, '$options': 'i' } }]
        // 'budgetDate': { $gte: startDate, $lte: endDate },
        "deleted": { $ne: 'true' }
    })
        .then(employee =>
            res.send({
                data: employee.map((i) => ({
                    _id: i._id,
                    budgetDate: i.budgetDate,
                    budgetNote: i.budgetNote,
                    budget: i.budget,
                    usedBudget: i.usedBudget,
                    sharedBudget: i.sharedBudget,
                    shared: i.shared === 'true' ? true : false,
                    usedBudget2: i.usedBudget !== undefined ? Number(i.usedBudget[0].split('/')[0]) : 0,
                    mealDate: moment(i.budgetDate).format('LL')



                }))
            })
            // res.json(employee)
        )
        .catch(err => res.status(400).json('Error :' + err));
});

router.route('/check/mealTodayWeekly').get((req, res) => {
    let startDate = moment(req.query.startDate).format('YYYY-MM-DD');
    let endDate = moment(req.query.endDate).format('YYYY-MM-DD');
    Meals.find({
        'mealDate': { $gt: startDate, $lt: endDate },
        "deleted": { $ne: 'true' }
    }).sort({ 'mealDate': -1 })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));

});

// add mealRecords
router.route('/add/mealRecord').post((req, res) => {

    const user = req.body.user;
    const meal = req.body.meal;
    const mealType = req.body.mealType;
    const mealCategory = req.body.mealCategory;

    const availing = req.body.availing;
    const feedback = req.body.feedback;
    const ingToAdd = req.body.ingToAdd ? req.body.ingToAdd : [];
    const ingToRemove = req.body.ingToRemove ? req.body.ingToRemove : [];


    const newEmployeeMealRecord = new MealRecords({ user, meal, mealType, mealCategory, availing, feedback, ingToAdd, ingToRemove });
    console.log('ewEmployeeMealRecord', newEmployeeMealRecord)
    newEmployeeMealRecord.save()
        .then(employee => res.json('New MealRecord Added!'))
        .catch(err => res.status(400).json('Error :' + err));

});

router.route('/updateMealRecords/:id').post((req, res) => {

    MealRecords.findById((req.params.id))
        .then(mealFeed => {
            mealFeed.user = req.body.user;
            mealFeed.meal = req.body.meal;
            mealFeed.mealType = req.body.mealType;
            mealFeed.mealCategory = req.body.mealCategory;

            mealFeed.availing = req.body.availing;
            mealFeed.feedback = req.body.feedback;
            mealFeed.ingToAdd = req.body.ingToAdd;
            mealFeed.ingToRemove = req.body.ingToRemove;

            mealFeed.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

});

router.route('/add/updatemealRecord').post((req, res) => {
    const mealRecordId = req.body.mealRecordId;

    MealRecords.updateOne(
        { _id: ObjectId(mealRecordId) },
        { $set: { "availed": true } }
    ).then(employee => res.json('Meal Serve Updated!'))
        .catch(err => res.status(400).json('Error :' + err));
});

// add mealRecords
router.route('/add/mealRecord2').post((req, res) => {

    const user = req.body.user;
    const meal = req.body.meal;
    const mealType = req.body.mealType;
    const mealRecordId = req.body.mealRecordId;
    var noOfServed;
    var result;
    var array = [];




    // MealRecords.aggregate( [
    //     // { $match: { "meal": { $eq: ObjectId(meal)}, "mealType": { $eq: mealType} } },
    //     // {  count: 'no'  }
    //     {
    //         $match: { "meal": { $eq: ObjectId(meal)}, "mealType": { $eq: mealType} }
    //       },
    //       {
    //         $count: "count"
    //       }
    //   ] ).then(employee => console.log('/////////////////////',  employee))
    //   .catch(err => res.status(400).json('Error: '+ err));

    async function listCars() {
        // let db = await MongoClient.connect(url);
        // let dbo = db.db("testdb");
        return await MealRecords.aggregate([
            // { $match: { "meal": { $eq: ObjectId(meal)}, "mealType": { $eq: mealType} } },
            // {  count: 'no'  }
            {
                $match: { "meal": { $eq: ObjectId(meal) }, "availed": { $eq: "true" }, "mealType": { $eq: mealType } }
            },
            {
                $count: "count"
            }
        ])
    }

    listCars().then(cars => {
        if (cars.length === 0) {
            noOfServed = 0;
        } else {
            noOfServed = cars[0]['count'];
        }

        console.log('mealll served', noOfServed)

        Meals.updateOne(
            { _id: ObjectId(meal) },
            { $set: { "mealServed": noOfServed } }
        ).then(employee => res.json('Meal Serve Updated!'))
            .catch(err => res.status(400).json('Error :' + err));
    })




});


// update

router.route('/updateAttendance/:id').post((req, res) => {
    Attendance.findById((req.params.id))
        .then(attendance => {
            attendance.user = req.body.user;
            attendance.timeIn = req.body.timeIn;
            attendance.timeOut = req.body.timeOut;
            attendance.dateRecorded = req.body.dateRecorded;

            attendance.save()
                .then(() => res.json('Record was updated!'))
                .catch(err => res.status(400).json('Error :' + err));
        })
        .catch(err => res.status(400).json('Error: ' + err));

});

// details meals
router.route('/get/getAttendance').get((req, res) => {
    let date = req.query.date;
    let id = req.query.id;
    console.log('heyyyyy', date, id)
    Attendance.find({ "user": { $eq: ObjectId(id) }, "dateRecorded": date })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));
});

//Add time
// add mealRecords
router.route('/add/time').get((req, res) => {

    const morningFrom = req.query.morningFrom;
    const morningTo = req.query.morningTo;
    const afternoonFrom = req.query.afternoonFrom;
    const afternoonTo = req.query.afternoonTo;

    // const newTime = new Time({morningFrom, morningTo, afternoonFrom, afternoonTo});

    // newTime.save()
    //     .then(employee => res.json('New Time Added!'))
    //     .catch(err => res.status(400).json('Error :' + err));
    Time.updateOne(
        { _id: ObjectId('623eb21cafb29301b4b14f37') },
        { $set: { "morningFrom": morningFrom, "morningTo": morningTo, "afternoonFrom": afternoonFrom, "afternoonTo": afternoonTo } }
    ).then(employee => res.json('Succesfully add Time'))
        .catch(err => res.status(400).json('Error :' + err));

});

//get meal today
router.route('/time/timeSet').get((req, res) => {

    Time.find({ "_id": { $eq: ObjectId('623eb21cafb29301b4b14f37') } })
        .then(employee => res.json(employee))
        .catch(err => res.status(400).json('Error :' + err));



});
module.exports = router;
